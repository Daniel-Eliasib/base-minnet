import pandas as pd
import os
import datetime
import matplotlib.pyplot as plt

data = pd.read_csv(os.path.join(os.path.dirname(os.path.realpath(__file__)), "..","Data","Casos_Diarios_Estado_Nacional_Confirmados_20200628.csv"))

print(data.get("nombre").get(32))

initialDate = datetime.date(2020,1,12)
finalDate = datetime.date(2020,6,25)
oneDayIncrement = datetime.timedelta(days=1)

currentDate = initialDate

day = 0

X = []
Y = []

while True:
    currentDate = currentDate + oneDayIncrement
    print("Day: " + str(currentDate) + " | Cases: " + str(data.get(currentDate.strftime("%d-%m-%Y")).get(32)))

    day = day + 1
    X.append(day)

    if day > 1:
        Y.append(data.get(currentDate.strftime("%d-%m-%Y")).get(32) + Y[day - 2])
    else:
        Y.append(data.get(currentDate.strftime("%d-%m-%Y")).get(32))
    

    if currentDate == finalDate:
        break

plt.plot(X, Y, "*b")
plt.show()

# print(date + datetime.timedelta(days=25))
# print(data.get(date.strftime("%d-%m-%Y")).get(32))