import numpy as np
import tensorflow as tf
#from tensorflow import keras
import tensorflow_core.python.keras.api._v2.keras as keras
from tensorflow_core.python.keras.api._v2.keras import layers
import scipy.io
import matplotlib.pyplot as plt
import time


np.random.seed(123)
#tf.random.set_seed(456654654)

data = scipy.io.loadmat('Data/New Equation/tyData_Normal/data.mat')

t_raw = data['t']
y_raw = data['y']*15

t_raw = t_raw.flatten()[:, None]
y_raw = y_raw.flatten()[:, None]

t_raw = t_raw.astype('float32')
y_raw = y_raw.astype('float32')

idx = np.random.choice(t_raw.shape[0], 250, replace=False)

idx.sort()

t_train = t_raw[idx, :]
y_train = y_raw[idx, :]

#* Creating the datasets
train_dataset = tf.data.Dataset.from_tensor_slices((t_train, y_train))
print(train_dataset)
train_dataset = train_dataset.shuffle(len(t_raw)*2)
train_dataset = train_dataset.batch(32)
# print(train_dataset)

model = keras.Sequential()

model.add(layers.Dense(1, input_shape=(1,), kernel_initializer=tf.random_normal_initializer(mean=0.0, stddev=0.1), bias_initializer='random_normal'))
model.add(layers.Dense(30, activation='relu', kernel_initializer=tf.random_normal_initializer(mean=0.0, stddev=0.1), bias_initializer=tf.random_normal_initializer(mean=0.0, stddev=0.1)))
model.add(layers.Dense(30, activation='relu', kernel_initializer=tf.random_normal_initializer(mean=0.0, stddev=0.1), bias_initializer=tf.random_normal_initializer(mean=0.0, stddev=0.1)))
model.add(layers.Dense(30, activation='relu', kernel_initializer=tf.random_normal_initializer(mean=0.0, stddev=0.1), bias_initializer=tf.random_normal_initializer(mean=0.0, stddev=0.1)))
model.add(layers.Dense(30, activation='relu', kernel_initializer=tf.random_normal_initializer(mean=0.0, stddev=0.1), bias_initializer=tf.random_normal_initializer(mean=0.0, stddev=0.1)))
model.add(layers.Dense(1, kernel_initializer=tf.random_normal_initializer(mean=0.0, stddev=0.1), bias_initializer=tf.random_normal_initializer(mean=0.0, stddev=0.1)))

A = model.predict(t_train)

plt.plot(t_train, A, 'r')

model.compile(optimizer=keras.optimizers.Adam(0.001), loss='mse', metrics=['mae'])

A = model.predict(t_train)

plt.plot(t_train, A, 'b')

plt.show()

optimizer = keras.optimizers.Adam(0.001)
loss_fn = keras.losses.MeanSquaredError()

epochs = 80000

@tf.function
def train_step(inputs, outputs):
    with tf.GradientTape() as tape:
        pred = model(inputs)
        loss = loss_fn(pred, outputs)
        loss = loss + sum(model.losses)
    grads = tape.gradient(loss, model.trainable_weights)
    
    optimizer.apply_gradients(zip(grads, model.trainable_weights))



for epoch in range(epochs):    
    for step, (x_batch_train, y_batch_train) in enumerate(train_dataset):
        train_step(x_batch_train, y_batch_train)
        #print("step: " + str(step))
    print("epoch" + str(epoch))




plt.plot(t_train, y_train, 'r')
plt.show()

start_time = time.time()
# model.fit(train_dataset, epochs=100)
total_time = time.time() - start_time
print("total time: " + str(total_time))

y_pred = model.predict(t_train)

plt.plot(t_train, y_train, 'r')
plt.plot(t_train, y_pred, 'b')
plt.show()