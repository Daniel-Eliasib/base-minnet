import logging, os

logging.disable(logging.WARNING)
os.environ["TF_CPP_MIN_LOG_LEVEL"] = "3"

import tensorflow as tf
from tensorflow import keras as keras
from tensorflow_core.python.keras.api._v2.keras import layers
# import keras
# from keras import layers
import numpy as np
import datetime
from os.path import join as pjoin
import io
import matplotlib.pyplot as plt
from prompt_toolkit.shortcuts import ProgressBar
from prompt_toolkit.key_binding import KeyBindings

# Create custom key bindings first
cancelArr = [False]

kb = KeyBindings()

@kb.add('x')
def _(event):
    "Caneling"
    cancelArr[0] = True

class MultiMINNet():
    def __init__(self, layerStructure, learningRateNet, learningRateOp, logDir, randomSeed: int):
        super().__init__()
        
        tf.random.set_seed(1254)

        if len(layerStructure) < 3:
            raise Exception("Net needs at least 3 layers.")
        for i in range(len(layerStructure)):
            if layerStructure[i] <=0:
                raise Exception("Layers MUST have at least 1 neuron.")
        
        self.baseLearningRate = learningRateNet
        self.learningRateNet = tf.Variable(self.baseLearningRate)
        self.learningRateOp = tf.Variable(learningRateOp)

        self.layerStructure = layerStructure

        # Type of MODEL
        self.Model = keras.Sequential(name="MINNet")
        
        # Build Input layer
        self.Model.add(layers.Dense(layerStructure[0], input_shape=(layerStructure[0],), dtype=tf.float32, kernel_initializer=tf.random_normal_initializer(mean=0.0, stddev=0.1), bias_initializer=tf.random_normal_initializer(mean=0.0, stddev=0.1)))

        # Add Hidden Layers
        for i in range(1, len(layerStructure)-1):
            self.Model.add(layers.Dense(layerStructure[i], activation=keras.activations.relu, dtype=tf.float32, kernel_initializer=tf.random_normal_initializer(mean=0.06, stddev=0.1), bias_initializer=tf.random_normal_initializer(mean=0.01, stddev=0.1)))
        
        # Output layer
        self.Model.add(layers.Dense(layerStructure[-1], dtype=tf.float32, kernel_initializer=tf.random_normal_initializer(mean=0.0, stddev=0.1), bias_initializer=tf.random_normal_initializer(mean=0.0, stddev=0.1)))

        # Loss function
        self.lossFunction = keras.losses.MeanSquaredError()

        # Optimizer
        self.netOptimizer = keras.optimizers.Adam(self.learningRateNet)
        self.opOptimizer = keras.optimizers.Adam(self.learningRateOp)

        # Metrics
        self.mainNNetMetric = keras.metrics.Mean()
        self.mainDiffOpMetric = keras.metrics.Mean()
        self.sumMetric = keras.metrics.Mean()
        
        # TensorBoard
        self.current_day = datetime.datetime.now().strftime("%Y-%m-%d")
        self.log_dir = logDir

        # Saving Info
        self.startTime = ""
        self.trainLogPath = ""
        self.weightLogPath = ""
        self.infoLogPath = ""
        self.mainPath = ""
        self.graphsImage = ""
    
        # self.Model.compile(optimizer=self.netOptimizer, loss = self.lossFunction)

        self.trainWithDiffOperator = False
    
    def SetDiffOperatorVariables(self, trainableVariables):
        self.opVariables = trainableVariables

    def SetDiffOperator(self, F):
        self.F = F

    def SetLearningRate(self, learningRate):
        self.learningRateNet.assign(learningRate)
    
    @tf.function
    def __SolutionTrainStep(self, X, Y):
        with tf.GradientTape() as tape:
            outNet = self.Model(X)
            lossValue = self.lossFunction(Y, outNet)
            lossValue += sum(self.Model.losses)
        
        grads = tape.gradient(lossValue, self.Model.trainable_variables)
        self.netOptimizer.apply_gradients(zip(grads, self.Model.trainable_variables))
        self.mainNNetMetric(lossValue)

    @tf.function
    def __DiffOpTrainStep(self, inputs, derivOrder: int):
        with tf.GradientTape(persistent=True) as tape:
            tape.watch(inputs)
            outputs = self.Model(inputs)
            derivatives = list()
            #List of gradients with respect of each varaible

            outputs_inputs = tape.gradient(outputs, inputs)
            derivatives.append(outputs_inputs)

            for i in range(derivOrder - 1):
                outputs_inputs = tape.gradient(outputs_inputs, inputs)
                derivatives.append(outputs_inputs)

            F_val = self.F(inputs, outputs, derivatives)

            lossValue = self.lossFunction(0, F_val)
            
        # Gradients
        trainVars = self.opVariables + self.Model.trainable_variables

        # Net Gradient
        grads = tape.gradient(lossValue, trainVars)

        self.netOptimizer.apply_gradients(zip(grads, trainVars))
        self.mainNNetMetric(lossValue)

        del tape

    @tf.function
    def __Trace(self):
        X = tf.convert_to_tensor([[1]], dtype=tf.float32)
        self.Model(X)
    
    def HybridTrain(self, iterations, trainDataset, partitionDataset, rawData: list, plottingRate: int, axLim : list):
        os.system('cls')

        t_raw = rawData[0]
        y_raw = rawData[1]

        X = rawData[2]

        #|| Log paths
        current_time = datetime.datetime.now().strftime("%H-%M")
        self.startTime = self.current_day + '_' + current_time + '(Hybrid)'

        self.trainLogPath = self.log_dir + '/Exp-' + self.startTime + '/train'
        self.graphsImage = self.log_dir + '/Exp-' + self.startTime + '/graphs'
        self.weightLogPath = self.log_dir + '/Exp-' + self.startTime + '/weights/CheckPoint-{epoch:04d}.ckpt'
        self.infoLogPath = self.log_dir + '/Exp-' + self.startTime + '/Info.txt'
        self.mainPath = self.log_dir + '/Exp-' + self.startTime

        #|| Summary
        train_summary_writer = tf.summary.create_file_writer(self.trainLogPath)
        
        #|| Solution figure creation
        fig = plt.figure(num="Neural Network Output")
        plt.axes()

        plt.plot(t_raw, y_raw, 'b')
        plt.plot(t_raw, y_raw, 'r*')

        plt.xlim([axLim[0], axLim[1]])
        plt.ylim([axLim[2], axLim[3]])

        plt.draw()
        plt.pause(0.000001)

        with ProgressBar(key_bindings=kb) as pb:
            for it in pb(range(iterations)):
                try:
                    #|| First we train the solution
                    dataEnumerate = enumerate(trainDataset)
                    for (batch, (netInput, netOutput)) in dataEnumerate:
                        self.__SolutionTrainStep(netInput, netOutput)

                    #|| Then we train the Differential operatior
                    dataEnumerate = enumerate(partitionDataset)
                    for (batch, dx) in dataEnumerate:
                        self.__DiffOpTrainStep(dx)

                    if it%plottingRate == 0:
                        data_Y_pred = self.Model.predict(X)
                        plt.cla()
                        plt.plot(t_raw, y_raw, 'b')
                        plt.plot(t_raw, y_raw, 'r*')
                        plt.plot(X, data_Y_pred, 'g')
                        plt.xlim([axLim[0], axLim[1]])
                        plt.ylim([axLim[2], axLim[3]])
                        #plt.draw()

                        #plt.pause(0.00000000001)

                        img = plot_to_image(fig)

                    with train_summary_writer.as_default():
                        tf.summary.scalar('MINNet Loss', self.mainNNetMetric.result(), step=it)
                        if it%plottingRate == 0:
                            tf.summary.image('Net Progress', img, step=it)
                        for i in range(len(self.opVariables)):
                            tf.summary.scalar(self.opVariables[i].name, self.opVariables[i].read_value().numpy()[0], step=it)

                    if it%500 == 0:
                        self.__SaveCheckPoint(self.weightLogPath, it)

                    self.mainNNetMetric.reset_states()

                    if cancelArr[0]:
                        break

                except KeyboardInterrupt:
                    print("Program stoped!")
                    break

        plt.close(fig)
        self.Save(self.weightLogPath, self.infoLogPath, it)

    def Save(self, weightPath : str, infoPath : str, lastIteration):
        self.__SaveCheckPoint(weightPath, lastIteration)
        infoFile = open(infoPath,"w+")
        netGraph = ""
        for i in range(len(self.layerStructure)):
            netGraph += "Dense[" + str(self.layerStructure[i]) + "]"
            if(i < len(self.layerStructure)-1):
                netGraph += "->"
        
        infoFile.write("GraphName: " + self.Model.name + "\n")
        infoFile.write("Graph: " + netGraph + "\n")
        infoFile.write("Number of iterations: " + str(lastIteration) + "\n")
        infoFile.write("Learning rate Net: " + str(self.learningRateNet) + "\n")
        infoFile.write("Learning rate Diff: " + str(self.learningRateOp) + "\n")
        for i in range(len(self.opVariables)):
            infoFile.write(self.opVariables[i].name + " = %s" % (self.opVariables[i].read_value().numpy()[0]) + "\n")
        
        infoFile.write("\n\n\nFor more info cheack tensorboard with direction: " + self.log_dir)
        infoFile.close()

    def __SaveCheckPoint(self, checkpoint_path : str, epoch):
        self.Model.save_weights(checkpoint_path.format(epoch=epoch))

# TO save the graph and see it's progress
def plot_to_image(figure):
    # Save the plot to a PNG in memory.
    buf = io.BytesIO()
    plt.savefig(buf, format='png')
    # Closing the figure prevents it from being displayed directly inside
    # the notebook.
    plt.close(figure)
    buf.seek(0)
    # Convert PNG buffer to TF image
    image = tf.image.decode_png(buf.getvalue(), channels=4)
    # Add the batch dimension
    image = tf.expand_dims(image, 0)
    return image